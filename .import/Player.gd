extends KinematicBody2D

export var speed = 9999.0
export var tileSize = 7.0
export var playing = true

onready var anim = $AnimatedSprite


var initpos = Vector2()
var dir = Vector2()	
var facing = "down"
var counter = 0.0

var anim1 = "downstat"

var moving = false

func _ready():
	initpos = position
	
func _process(delta):

	if not moving:
		set_dir()
	
	elif dir != Vector2():
		move(delta)
	else:
		moving = false
		
	if facing == "down":
		anim.play("downstat")

	elif facing == "up":
		anim.play("upstat")

		
	elif facing == "left":
		anim.play("leftstat")
	
	elif facing == "right":
		anim.play("rightstat")
	
		
func set_dir():#setdirection
	dir = get_dir()
	
	if dir.x != 0 or dir.y != 0:
		
		if dir.x >0:
			facing = "right"
			anim1 = "rightwalk"
			anim.play(anim1)
		elif dir.x < 0:
			facing = "left"
			anim1 = "leftwalk"
			anim.play(anim1)
		elif dir.y > 0:
			facing = "down"
			anim1 = "downwalk"
			anim.play(anim1)
		elif dir.y < 0:
			facing = "up"
			anim1 = "upwalk"
			anim.play(anim1)
		moving = true
		initpos = position
func get_dir(): #userinput
	var x = 0
	var y = 0
	
	if dir.y == 0:
		x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	if dir.x == 0:
		y = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
	return Vector2(x,y)


func move(delta): #movement
	counter += delta *speed
	
	if counter >= 1.0:
		position = initpos + dir *tileSize
		counter = 0.0
		moving = false
	else:
		position = initpos + dir * tileSize
	
		