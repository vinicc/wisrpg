
extends KinematicBody2D

var direction = Vector2(0,0)
var startPos = Vector2(0,0)
var moving = false
onready var anim = $AnimatedSprite

var prevvar = "down"

const SPEED = 1
const GRID = 16

var world

func _ready():
	set_physics_process(true)
	anim.play("downstat")

func _physics_process(delta): #movement
		
	if Input.is_action_pressed("ui_up"):
		anim.play("up")
		moving = true
		if(Input.is_key_pressed(KEY_X)):
			direction = Vector2(0, -4)
		else:
			direction = Vector2(0, -2)
		self.move_and_collide(direction)
		startPos = position
		prevvar = "up"
	elif prevvar == "up":
		anim.play("upstat")
			
	if Input.is_action_pressed("ui_down"):
		anim.play("down")
		moving = true
		if(Input.is_key_pressed(KEY_X)):
			direction = Vector2(0, 4)
		else:
			direction = Vector2(0, 2)
		self.move_and_collide(direction)
		startPos = position
		prevvar = "down"
	elif prevvar == "down":
		anim.play("downstat")

	if Input.is_action_pressed("ui_left"):
		anim.play("left")
		moving = true		
		if(Input.is_key_pressed(KEY_X)):
			direction = Vector2(-4, 0)
		else:
			direction = Vector2(-2, 0)
		self.move_and_collide(direction)
		startPos = position
		prevvar = "left"
	elif prevvar == "left":
		anim.play("leftstat")

	if Input.is_action_pressed("ui_right"):
		anim.play("right")
		moving = true
		if(Input.is_key_pressed(KEY_X)):
			direction = Vector2(4, 0)
		else:
			direction = Vector2(2, 0)
		
		self.move_and_collide(direction)
		startPos = position
		prevvar = "right"
	elif prevvar == "right":
		anim.play("rightstat")
