
extends KinematicBody2D

#movement and animation.

var direction = Vector2(0,0)
var startPos = Vector2(0,0)
var moving = false
onready var anim = $AnimatedSprite
onready var player = $"."

var prevvar = "downWalk"
var collide

func _ready():
	set_physics_process(true)
	anim.play("downStat")


func _physics_process(delta): 
	#movement
	#up
	if Input.is_action_pressed("ui_up"):
		#play walking animation
		anim.play("upWalk")
		if(Input.is_key_pressed(KEY_X)):
			#running
			direction = Vector2(0, -200)
		else:
			#walking
			direction = Vector2(0, -100)
		var collide = self.move_and_slide(direction)
		startPos = position
		prevvar = "upWalk"
	elif prevvar == "upWalk":
		#reset to static animation(with direction) after doing action. [animated sprite]
		anim.play("upStat")
	#down
	if Input.is_action_pressed("ui_down"):
		anim.play("downWalk")
		if(Input.is_key_pressed(KEY_X)):
			direction = Vector2(0, 200)
		else:
			direction = Vector2(0, 100)
		self.move_and_slide(direction)
		startPos = position
		prevvar = "downWalk"
	elif prevvar == "downWalk":
		anim.play("downStat")
	#left
	if Input.is_action_pressed("ui_left"):
		anim.play("leftWalk")
		if(Input.is_key_pressed(KEY_X)):
			direction = Vector2(-200, 0)
		else:
			direction = Vector2(-100, 0)
		self.move_and_slide(direction)
		startPos = position
		prevvar = "leftWalk"
	elif prevvar == "leftWalk":
		anim.play("leftStat")
	#right
	if Input.is_action_pressed("ui_right"):
		anim.play("rightWalk")
		if(Input.is_key_pressed(KEY_X)):
			direction = Vector2(200, 0)
		else:
			direction = Vector2(100, 0)
		self.move_and_slide(direction)
		startPos = position
		prevvar = "rightWalk"
	elif prevvar == "rightWalk":
		anim.play("rightStat")

#z index, layering
	z_index = player.get_global_position().y
	print(z_index)  #debug
